Soal 1 Membuat Database

CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database

table users: 
CREATE TABLE users( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255) );

table categories:
CREATE TABLE categories( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) );

table items:
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255), description varchar(255), price int(8), stock int(8), category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );

Soal 3 Memasukkan Data pada Table

table users: 
INSERT INTO users(name, email, password) VALUES("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

table categories: 
INSERT INTO categories(name) VALUES("gadget"), ("cloth"), ("men"), ("women"), ("branded");

table items:
INSERT INTO items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);

Soal 4 Mengambil Data dari Database
Mengambil data users:
SELECT name, email FROM users;

Mengambil data items: 
(*) SELECT * FROM items WHERE price > 1000000;
(*) SELECT * FROM items WHERE name LIKE '%sang%';

Menampilkan data items join dengan kategori:
SELECT * FROM items JOIN categories ON items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
UPDATE items SET price = 2500000 WHERE name LIKE '%b50%';